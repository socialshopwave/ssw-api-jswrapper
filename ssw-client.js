SSWClient = function(consumer_key, consumer_secret, redirect_uri) {
  var _shop_domain = encodeURI(location.protocol + '//' + location.hostname);
  var _service_domain = 'api.growave.io';
  var _source = 'js';
  var _version = '2.0';
  var _scheme = 'https://';
  var _api_path = '/api/';
  var _authorize_path = 'authorize';
  var _access_token_path = 'access_token';

  var api_url, authorize_url, token, self;

  api_url = _scheme + _service_domain + _api_path;
  authorize_url = api_url + _authorize_path;

  self = this;
  this.getAuthorizationUri = function() {
    if (!redirect_uri) {
      throw new Exception('redirect_uri is not defined');
    }
    var data = {
      'response_type': 'code',
      'state': Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1),
      'client_id': consumer_key,
      'redirect_uri': redirect_uri
    };
    return authorize_url + '?' + jQuery.param(data);
  };

  this.getAppToken = function(scopes, callback) {
    if (typeof scopes == 'function') {
      callback = scopes;
    }
    if (typeof scopes != 'string' || !scopes) {
      scopes = 'read_user write_user read_wishlist write_wishlist read_review write_review read_gallery read_reward write_reward app_install';
    }

    var payload = {
      'client_id': consumer_key,
      'client_secret': consumer_secret,
      'scope': scopes,
      'grant_type': 'client_credentials',
    };

    self.request('POST', _access_token_path, payload, function(response) {
      var result = false;
      if (response && typeof response.access_token != 'undefined') {
        result = true;
        self.setAccessToken(response.access_token);
      }
      if (typeof callback == 'function') {
        callback(result);
      }
    });
  };

  this.setAccessToken = function(access_token) {
    token = access_token;
  };

  this.setRequiredGetParamsToUrl = function (method, path, params) {
    if (method !== 'GET') {
      path += '?gw_api_shop_domain=' + _shop_domain + '&gw_api_source=' + _source + '&gw_api_version=' + _version;
    } else {
      params.gw_api_shop_domain = _shop_domain;
      params.gw_api_source = _source;
      params.gw_api_version = _version;
    }

    if (method === 'PUT' || method === 'DELETE') {
      path += '&' + jQuery.param(params);
      delete params.access_token;
    }

    return [path, params];
  }

  this.call = function(method, path, params, callback) {
    params.access_token = token;
    [path, params] = self.setRequiredGetParamsToUrl(method, path, params);
    var url = api_url + path;
    if (typeof path == 'undefined') {
      throw new Exception('You must specify a request url.');
    }
    if (typeof path !== 'string') {
      throw new Exception('String expected, got ' + typeof path);
    }

    jQuery.ajax({
      'url': url,
      'method': method,
      'data': params
    }).always(function(data, textStatus, jqXHR) {
      if (typeof callback == 'function') {
        callback(data, textStatus, jqXHR);
      }
    });
  };

  function getValidCallback(callback, arguments) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    return callback;
  }

  this.request = function(method, path, params, callback) {
    [path, params] = self.setRequiredGetParamsToUrl(method, path, params);
    var url = api_url + path;

    jQuery.ajax({
      'url': url,
      'method': method,
      'data': params
    }).always(function(data, textStatus, jqXHR) {
      if (typeof callback == 'function') {
        callback(data, textStatus, jqXHR);
      }
    });
  };

  this.getUsers = function(limit, page, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }

    self.call('GET', 'users', {'limit': limit, 'page': page}, callback);
  };

  this.getUser = function(user_id, callback) {
    self.call('GET', 'users/' + user_id, {}, callback);
  };

  this.editUser = function(user_id, user_data, callback) {
    self.call('PUT', 'users/' + user_id, user_data, callback);
  };

  this.searchUser = function(email, callback) {
    self.call('GET', 'users/search', {'field': 'email', 'value': email}, callback);
  };

  this.getWishlist = function(user_id, board_id, limit, page, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof board_id !== 'number' || !board_id) {
      board_id = 0;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }
    var data = {'user_id': user_id, 'board_id': board_id, 'limit': limit, 'page': page};
    self.call('GET', 'wishlist', data, callback);
  };

  this.addToWishlist = function(user_id, product_id, board_id, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof board_id !== 'number' || !board_id) {
      board_id = 0;
    }
    var data = {'user_id': user_id, 'product_id': product_id, 'board_id': board_id};
    self.call('POST', 'wishlist', data, callback);
  };

  this.removeFromWishlist = function(user_id, product_id, callback) {
    callback = getValidCallback(callback, arguments);
    var data = {'user_id': user_id, 'product_id': product_id};
    self.call('DELETE', 'wishlist', data, callback);
  };

  this.createBoard = function(user_id, title, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof title !== 'string' || !title) {
      title = 'FAVORITES';
    }
    var data = {'user_id': user_id, 'title': title};
    self.call('POST', 'wishlist/boards', data, callback);
  };

  this.getBoards = function(user_id, limit, page, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }
    var data = {'user_id': user_id, 'limit': limit, 'page': page};
    self.call('GET', 'wishlist/boards', data, callback);
  };

  this.editBoard = function(user_id, board_id, title, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof board_id !== 'number' || !board_id) {
      board_id = 0;
    }
    if (typeof title !== 'string' || !title) {
      title = 'FAVORITES';
    }
    var data = {'user_id': user_id, 'board_id': board_id, 'title': title};
    self.call('PUT', 'wishlist/boards/' + board_id, data, callback);
  };

  this.removeBoard = function(user_id, board_id, callback) {
    callback = getValidCallback(callback, arguments);
    var data = {'user_id': user_id, 'board_id': board_id};
    self.call('DELETE', 'wishlist/boards', data, callback);
  };

  this.moveProduct = function(user_id, product_id, current_board_id, destination_board_id, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof product_id !== 'number' || !product_id) {
      product_id = 0;
    }
    if (typeof current_board_id !== 'number' || !current_board_id) {
      current_board_id = 0;
    }
    if (typeof destination_board_id !== 'number' || !destination_board_id) {
      destination_board_id = 0;
    }
    var data = {'user_id': user_id, 'product_id': product_id, 'current_board_id': current_board_id, 'destination_board_id': destination_board_id };
    self.call('PUT', 'wishlist/moveProduct/', data, callback);
  };

  this.reviews = function(user_id, product_id, limit, page, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof product_id !== 'number' || typeof product_id !== 'string' || !product_id) {
      product_id = null;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }

    var data = {'user_id': user_id, 'limit': limit, 'page': page};
    if (product_id !== null) {
      data.product_id = product_id;
    }
    self.call('GET', 'reviews', data, callback);
  };

  this.postReview = function(email, name, body, rate, product_id, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof email !== 'string' || !email) {
      email = '';
    }
    if (typeof name !== 'string' || !name) {
      name = '';
    }
    if (typeof body !== 'string' || !body) {
      body = '';
    }
    if (typeof rate !== 'number' || !rate) {
      rate = 0;
    }
    if (typeof product_id !== 'number' || typeof product_id !== 'string' || !product_id) {
      product_id = 0;
    }

    var data = {'email': email, 'name': name, 'body': body, 'rate': rate, 'product_id': product_id};
    self.call('POST', 'reviews', data, callback);
  };

  this.rate = function(product_ids, limit, page, callback) {
    callback = getValidCallback(callback, arguments);
    if (typeof product_ids == 'undefined' || typeof product_ids == 'function' || !product_ids) {
      product_ids = null;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }

    var data = {'limit': limit, 'page': page};
    if (product_ids !== null) {
      data.product_ids = product_ids;
    }

    self.call('GET', 'reviews/products', data, callback);
  };

  this.getGalleries = function (limit, page, callback) {
    self.call('GET', 'galleries', {limit: limit, page: page}, callback);
  };

  this.getMedia = function (productId, galleryId, limit, page, callback) {
    self.call('GET', 'media', {product_id: productId, gallery_id: galleryId, limit: limit, page: page}, callback);
  };

  this.getQA = function (itemId, limit, page, callback) {
    self.call('GET', 'qa/' + itemId, {limit: limit, page: page}, callback);
  };

  this.earnReward = function (user_id, email, rule_type, points) {
    user_id = user_id || 0;
    email = email || null;
    rule_type = rule_type || '';
    points = points || 0;
    self.call('POST', 'reward/earn', {user_id: user_id, email: email, rule_type: rule_type, points: points});
  };

  this.getEarningRules = function (userId, all, callback) {
    userId = userId || 0;
    all = all || 0;
    self.call('GET', 'reward/earningRules', {user_id: userId, all: all}, callback);
  };

  this.redeemReward = function (userId, email, ruleId, amount, callback) {
    userId = userId || 0;
    email = email || "";
    ruleId = ruleId || 0;
    amount = amount || 0;
    self.call('POST', 'reward/redeem', {user_id: userId, email, rule_id: ruleId, amount}, callback);
  };  

  this.install = function (themeId) {
    themeId = themeId || 0;
    self.call('GET', 'install', {theme_id: themeId}, callback);
  };
};